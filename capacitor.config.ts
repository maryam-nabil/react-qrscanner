import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'io.ionic.starter',
  appName: 'empty',
  webDir: 'build',
  bundledWebRuntime: false
};

export default config;
