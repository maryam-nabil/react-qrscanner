import './ExploreContainer.css';

const Form = require('@penta-b/mna-penta-smart-forms');
interface ContainerProps { }

const ExploreContainer: React.FC<ContainerProps> = () => {
  let data = {};
let actions = {
    "submit": () =>  new Promise(function(myResolve, myReject) {
        console.log(data);
    })
};
const schema29 = {
    "groups": {
        "16424390068051626092098655": {
            "ordered": true,
            "settings": [],
            "groupType": "main",
            "id": "16424390068051626092098655",
            "type": "simple",
            "title": "",
            "fields": {
                "1646060332670": {
                    "label": "aaa",
                    "placeholder": "aaa",
                    "type": "qr",
                    "validations": [],
                }
            }
        }
    },
    "title": "",
    "buttons": {
        "submit": {
            "type": "submit"
        }
    }
};
  return (
    <div className="container">
      <strong>Ready to create an app?</strong>
      <Form schema={schema29}
                                     data={data}
                                     actions={actions}
/>;
      <p>Start with Ionic <a target="_blank" rel="noopener noreferrer" href="https://ionicframework.com/docs/components">UI Components</a></p>
    </div>
    
  );
};

export default ExploreContainer;
